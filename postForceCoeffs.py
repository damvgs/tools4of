import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import glob
import os


def computeAoA(df, AoAi, ampl, omega):
    """
    This function reads a pandas dataframe, computes the AoA based on timesteps.
    eulerAngles = amplitude * np.sin(omega_*t)
    """
    AoA_df = AoAi - (amplitude * np.sin(omega * df.Time))
    return AoA_df

def time_normalizer(t):
    cycle_len = 4
    while t >= cycle_len:
        t -= cycle_len
    return t

def plot_ClCd_AoA(sim_df_lst, legend_lst, AoAi, exp_df=None, var2plt="Cl", fig_out_path=False):

    # color_lst = ["black", "blue", "green", "red"]
    color_lst = ["blue", "green", "red"]

    fig, ax1 = plt.subplots(figsize=(8, 6), dpi=100)

    ax1.set_xlabel('AoA')
    ax1.set_ylabel(var2plt)

    # Plotting simulated data
    for c, sim_df in enumerate(sim_df_lst):
        ax1.plot(sim_df.AoA, sim_df[var2plt], color=color_lst[c])

    # Plotting Experiment
    if exp_df is not None:
        ax1.plot(exp_df.AoA, exp_df[var2plt], color='black', marker='o', linestyle='None')
        # ax1.plot(exp_df.AoA, exp_df['Cl_pp'], color='blue', marker='o', linestyle='None')
        # ax1.plot(exp_df.AoA, exp_df['Cl_fb'], color='black', marker='o', linestyle='None')

        legend_lst.append('Exp')
        # legend_lst.append('Exp Static')
        # legend_lst.append('Exp_pp')
        # legend_lst.append('Exp_fb')

    ax1.tick_params(axis='y', 
                    # labelcolor=color
                    )
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    fig.legend(legend_lst, loc='upper left', bbox_to_anchor=(0.1, 0.95))

    if fig_out_path:
        plt.savefig(fig_out_path)
        plt.close()
    else:
        plt.show()


if __name__ == "__main__":

    # AoA_lst = [8, 14, 20]
    AoA_lst = [8]
    
    for AoAi in AoA_lst:

        amplitude = -10
        omega = 1.58

        m_res = "m"  # Mesh resolution (c, m, f)  

        # tgt_pth = f'io/forceCoeffs/osciS809SAvsSST_{m_res}'
        # tgt_pth = f'io/forceCoeffs/osciDUW2SAvsSST_{m_res}'
        steady_pth = 'io/forceCoeffs/DU_W2-250_Polars'
        bangga_pth = 'io/forceCoeffs/BanggaDSdata'
        duw2250_pth = 'io/forceCoeffs/DU_W2-250_DSdata'
        tgt_pth = 'io/forceCoeffs/D_URANS_kOmegaSSTLM'

        sa_pth = f'{tgt_pth}/forceCoeffsSA{str(AoAi).zfill(3)}.dat'
        # sst_pth = f'{tgt_pth}/forceCoeffsSST{str(AoAi).zfill(3)}.dat'
        sst_pth = f'{tgt_pth}/coefficient_0.dat'

        banggaSA_pth = f'{bangga_pth}/data_out_aoam_{AoAi}_SA.dat'
        banggaSST_pth = f'{bangga_pth}/data_out_aoam_{AoAi}_SST.dat'

        exp_pth = f'{bangga_pth}/data_out_aoam_{AoAi}_exp.dat'
        # exp_pth = f'{duw2250_pth}/du_w2_250_piyusch.csv'

        # Read data
        ## My simulated data - Dynamic Stall
        #raw_SAdata = pd.read_csv(sa_pth, delimiter=r"\s+", skiprows=9, dtype=float, names=['Time', 'Cm', 'Cd', 'Cl', 'Cl(f)', 'Cl(r)'])
        # raw_SSTdata = pd.read_csv(sst_pth, delimiter=r"\s+", skiprows=9, dtype=float, names=['Time', 'Cm', 'Cd', 'Cl', 'Cl(f)', 'Cl(r)'])
        # raw_SSTdata = pd.read_csv(sst_pth, delimiter=r"\s+", skiprows=13, dtype=float, names=['Time', 'Cm', 'Cd', 'Cl', 'Cl(f)', 'Cl(r)'])
        raw_SSTdata = pd.read_csv(sst_pth, delimiter=r"\s+", skiprows=13, dtype=float, names=['Time', 'Cd', 'Cs', 'Cl', 'CmRoll', 'CmPitch', 'CmYaw', 'Cd(f)', 'Cd(r)',	'Cs(f)', 'Cs(r)', 'Cl(f)', 'Cl(r)'])

        ## My simulated data - Steady polars

        steady_data = pd.DataFrame(columns=['AoA', 'Cm', 'Cd', 'Cl', 'Cl(f)', 'Cl(r)'])

        files_lst = sorted(glob.glob(f'{steady_pth}/*.dat'))
        for f, file in enumerate(files_lst):

            aoa = int(file[-7:-4])

            tmp_steady_data = pd.read_csv(file, delimiter=r"\s+", skiprows=9, dtype=float, names=['AoA', 'Cm', 'Cd', 'Cl', 'Cl(f)', 'Cl(r)']).tail(1)
            tmp_steady_data['AoA'] = aoa

            steady_data = steady_data.append(tmp_steady_data, ignore_index=True)

        ## Simualted data (Bangga)
        bangga_SAdata = pd.read_csv(banggaSA_pth, delimiter=r"\s+", skiprows=1, dtype=float, names=['AoA', 'Cl'])
        bangga_SSTdata = pd.read_csv(banggaSST_pth, delimiter=r"\s+", skiprows=1, dtype=float, names=['AoA', 'Cl'])

        ## Bangga's dynamic stall experiments
        exp_data = pd.read_csv(exp_pth, delimiter=r"\s+", skiprows=1, dtype=float, names=['AoA', 'Cl'])
        
        ## For reading Piyush's laminar static polars
        # exp_data = pd.read_csv(exp_pth, delimiter=",", dtype=float, usecols=['alpha', 'C_L pp'])
        # exp_data = pd.read_csv(exp_pth, delimiter=",", dtype=float, usecols=['alpha', 'C_L fb'])
        # exp_data = pd.read_csv(exp_pth, delimiter=",", dtype=float, usecols=['alpha', 'C_L pp', 'C_L fb'])
        # exp_data.columns = ['AoA', 'Cl_pp', 'Cl_fb']
        # exp_data.columns = ['AoA', 'Cl']
        # exp_data = exp_data.loc[(exp_data['AoA'] >= 0) & (exp_data['AoA'] <= 20)]

        # Compute AoA from time and oscilating function
        # raw_SAdata['AoA'] = computeAoA(raw_SAdata, AoAi, amplitude, omega)
        raw_SSTdata['AoA'] = computeAoA(raw_SSTdata, AoAi, amplitude, omega)
        
        if m_res in ['c', 'm', 'f']:
            
            # sa_data = raw_SAdata.loc[(raw_SAdata['Time'] >= 16) & (raw_SAdata['Time'] <= 20)]
            # sst_data = raw_SSTdata.loc[(raw_SSTdata['Time'] >= 16) & (raw_SSTdata['Time'] <= 20)]
            sst_data = raw_SSTdata.loc[(raw_SSTdata['Time'] >= 26) & (raw_SSTdata['Time'] <= 30)]

            # sa_data['Cl'] = sa_data['Cl'] * np.cos(sa_data['AoA']*np.pi/180)
            sst_data['Cl'] = sst_data['Cl'] * np.cos(sst_data['AoA']*np.pi/180)

        else:

            # Select data
            step = 0.001
            to_bin = lambda x: np.floor(x / step) * step

            

            sa_data = raw_SAdata.loc[(raw_SAdata['Time'] >= 8) & (raw_SAdata['Time'] <= 20)]

            sa_data.loc[:, 'AoA'] = sa_data.loc[:, 'AoA'].round(decimals=2)

            sa_data.loc[:, 'Time'] = sa_data.loc[:, 'Time'].map(time_normalizer)
            sa_data.loc[:, 'TimeBin'] = sa_data.loc[:, 'Time'].map(to_bin)
            # sa_data.loc[:, 'TimeBin'] = sa_data.loc[:, 'TimeBin'].map(time_normalizer)

            # sa_ave_data = sa_data.groupby(['AoAbin'], sort=False).mean() #.sort_values(by=['Time'])
            # sa_ave_data = sa_data.groupby(['TimeBin'], sort=False).mean() #.sort_values(by=['Time'])

            sa_ave = sa_data.groupby(['TimeBin']).mean() #.sort_values(by=['Time'])
            sa_ave.drop(['Time', 'Cl(f)', 'Cl(r)'], axis=1, inplace=True)
            sa_ave.rename_axis('Time')
            # sa_ave['Cl'].rolling(5).apply(np.mean)
            # sa_ave.loc[:, 'AoA'] = sa_ave.loc[:, 'AoA'].round(decimals=2)

            # sa_ave['TimeBin'] = sa_ave.Time.map(to_bin)
            print(sa_ave)
            plt.plot(sa_data.AoA, sa_data.Cl, label='SA_data')
            plt.plot(sa_ave.AoA, sa_ave.Cl, label="SA_ave")
            plt.plot(sa_data.AoA, sa_data.Cl.rolling(50).apply(np.mean), label="SA_data_roll")
            plt.legend()
            plt.show()
            exit()


            sst_data = raw_SSTdata.loc[(raw_SSTdata['Time'] >= 16) & (raw_SSTdata['Time'] <= 20)]

            # Correcting Cl from AoA0 to current AoA
        
            sa_ave['Cl'] = sa_ave['Cl'] * np.cos(sa_ave['AoA']*np.pi/180)

            # sst_data['Cl'] = sst_data['Cl'] * np.cos(sst_data['AoA']*np.pi/180)

        # plt.plot(sa_data['AoA'], sa_data['Cl'])
        # plt.plot(sst_data['AoA'], sst_data['Cl'])
        # # plt.plot(sel_data['Time'], sel_data['AoA'])
        # plt.show()
        # plt.close()

        # Plot Cl vs AoA
        
        # Only Cl data from Bangga
        var2plt = "Cd"

        sim_df_lst = [
                    #   sa_data,
                      #sa_ave,
                      sst_data, 
                    #   steady_data
                    #   bangga_SAdata, 
                    #  bangga_SSTdata
                      ]

        legend_lst = [
                    #   "SA", 
                      "SST", 
                    #   "Sim Static"
                    #   "SA-Bangga", 
                    #  "SST-Bangga"
                      ]

        out_pth = "io/figures"
        # fig_out_path = f'{out_pth}/{m_res}_{var2plt}_AoA{AoAi}_bangga.png'
        # fig_out_path = f'{out_pth}/{m_res}_{var2plt}_AoA{AoAi}_DUW2250_Piyush.png'
        # fig_out_path = f'{out_pth}/{m_res}_{var2plt}_AoA{AoAi}_DUW2250.png'
        fig_out_path = f'{out_pth}/arslan_test.png'


        # plot_ClCd_AoA(sim_df_lst, exp_df, legend_lst, AoAi, var2plt="Cl", fig_out_path=fig_out_path)
        # plot_ClCd_AoA(sim_df_lst, legend_lst, AoAi, exp_df=exp_data, var2plt="Cl")
        # plot_ClCd_AoA(sim_df_lst, legend_lst, AoAi, exp_df=exp_data, var2plt="Cl", fig_out_path=fig_out_path)
        plot_ClCd_AoA(sim_df_lst, legend_lst, AoAi, var2plt=var2plt)
        plot_ClCd_AoA(sim_df_lst, legend_lst, AoAi, var2plt='Cl')
        # plot_ClCd_AoA(sim_df_lst, legend_lst, AoAi, var2plt="Cl", fig_out_path=fig_out_path)
