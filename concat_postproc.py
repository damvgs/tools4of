import pandas as pd
import argparse
import glob


def concat_pp_data(pp_dir_pth):
    """
    This function reads and concatenates all files from the postProcessing directory.

    pp_dir_pth : str (Path to the desired directory)
    """

    time0 = 0.0

    out_pth = f'{pp_dir_pth}/forceCoeffs.dat'

    #TODO fill timestamps-directory-names with zeros so that 'sorted()' works properly
    list_dir = sorted(glob.glob(f'{pp_dir_pth}/*'))

    if out_pth in list_dir:
        list_dir.remove(out_pth)

    # Open output file to write on
    with open(out_pth, 'w') as outfile:
        
        # Loop though all timestep directories
        for file_pth in list_dir:

            print(f'## Reading --->  {file_pth}')
 
            # Read in forceCoeffs.dat files
            with open(f'{file_pth}/forceCoeffs.dat', 'r') as infile:

                for line in infile:

                    if line[0] == '#':
                        if time0 == 0.0:
                            outfile.write(line)
                        else:
                            continue
                    else:
                        time = float(line.split()[0])
                        if time <= time0 or len(line.split()) != 6:
                            continue
                        else:
                            outfile.write(line)

                # Update time0 with last timestep
                time0 = float(line.split()[0])

    print(f'Done writing output file at: {out_pth}')
    

if __name__ == '__main__':

    # parser = argparse.ArgumentParser()
    # parser.add_argument('forceCoeffs_pth', help='Path to forceCoeffs directory.', type=str)
    # args  = parser.parse_args()

    # forceCs_pth = args.forceCoeffs_pth
    # forceCs_pth = 'io/forceCoeffs/osciS809SAvsSST_f/forces_ko'
    forceCs_pth = 'io/forceCoeffs/osciS809SAvsSST_f/forces_sa'

    concat_pp_data(forceCs_pth)


