import pandas as pd
import os
import argparse

parser = argparse.ArgumentParser(description='Translate all airfoil coordinates to 0.25*C. '\
                                             'Enter path to .dat file with airfoil coordinates with origin in (0, 0). '
                                             'The program will shift all X coordinates, so the origin will be in (1/4 Chord, 0).')
parser.add_argument('inputPath', type=str, help='Path to coordinates input file (.dat).')

args = parser.parse_args()
input_pth = os.path.realpath(args.inputPath)

# input_pth = "/home/danielvonglehn/dev/s809.dat"

af_name = open(input_pth,"r").readlines()[0]

# Reading airfoil coords
af_points = pd.read_csv(input_pth, delimiter=r"\s+", skiprows=1, dtype=float, names=['X', 'Y'])

# Chord length
chord = af_points['X'][0]

# Move all x-coords to 0.25C

af_points['X'] = af_points['X'] - 0.25*chord

# Getting dir path and file name
main_pth = os.path.dirname(input_pth)
file_name = os.path.basename(input_pth)

# Creating new file name
output_pth = '{}/{}_025C.dat'.format(main_pth, file_name[:-4])

# Remove preview existing file
if os.path.exists(output_pth):
    os.remove(output_pth)

# Writing data to new file
with open(output_pth, "w+") as f:

    # Writing Airfoil name as header
    f.write("{}".format(af_name))

    # Writing Airfoil points
    af_points.to_csv(f, header=None, index=None, sep="\t", float_format='%.6f')
